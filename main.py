import Read as Rd
import pandas as pd
import datetime
import numpy as np
from matplotlib import pyplot as plt

import openpyxl


#--------- Insert Modelled Data ---------#

Dat_Model_Time, Dat_Model_Rn = Rd.File("radon.nc", 10, 4)
Modelled = Dat_Model_Rn*6.9795*10**18

#--------- Insert Observed Data ---------#

Datos = pd.read_csv('Rn_CarmenRabida.csv')

Dat_Experimental_Date = Datos['Date'].to_numpy()

Datos['Reference'] = '1900-01-01 00:00:00'
Datos['Reference'] = pd.to_datetime(Datos['Reference'])
Datos['Date'] = pd.to_datetime(Datos['Date'])


Datos['Minutes'] = ((Datos['Date'] - Datos['Reference']).dt.days * 24 +
                    (Datos['Date'] - Datos['Reference']).dt.seconds / 3600)

Datos['Minutes'] = Datos['Minutes'].astype(int)

Dat_Experimental_Time = Datos['Minutes'].to_numpy()

Datos.set_index(Datos['Minutes'], inplace=True)
Datos.drop(['Date', 'Minutes', 'Reference'],axis=1,inplace=True)

El_Carmen = Datos['El-Carmen'].to_numpy()
La_Rabida = Datos['La-Rabida'].to_numpy()

Observed_Carmen = np.zeros(len(Dat_Model_Time))
Observed_Rabida = np.zeros(len(Dat_Model_Time))
Finish_Date = np.zeros(len(Dat_Model_Time), dtype = object)

for i in range(0, len(Dat_Model_Time)):
    Position = np.array(np.where(Dat_Experimental_Time == Dat_Model_Time[i]))
    Position = np.array(Position[0])

    Observed_Carmen[i] = El_Carmen[int(Position)]
    Observed_Rabida[i] = La_Rabida[int(Position)]
    Finish_Date[i] = Dat_Experimental_Date[int(Position)]

print(Finish_Date)
Time = [datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S").date() for date in Finish_Date]

plt.subplot(1, 2, 1)
plt.plot(Time, Observed_Carmen, '-b', label = 'Observed Carmen')
plt.plot(Time, Modelled, '-r', label = 'Modelled')
plt.title("Comparation in [37.2703 N 6.9243 W] vs [37.25 N 7 W]")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)

plt.subplot(1, 2, 2)
plt.plot(Time, Observed_Rabida, '-b', label = 'Observed Rabida')
plt.plot(Time, Modelled, '-r', label = 'Modelled')
plt.title("Comparation in [37.2029 N 6.9201 W] vs [37.25 N 7 W]")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)

#---------------- Comparation between both variables ----------------#
print("----------------------------------------------------------")
print("----------------- Results for Carmen ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE = Rd.RMSE(Modelled, Observed_Carmen)
MensajeBIAS = Rd.BIAS(Modelled, Observed_Carmen)
MensajeIOA = Rd.IOA(Modelled, Observed_Carmen)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")

print("----------------------------------------------------------")
print("----------------- Results for Rabida ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE = Rd.RMSE(Modelled, Observed_Rabida)
MensajeBIAS = Rd.BIAS(Modelled, Observed_Rabida)
MensajeIOA = Rd.IOA(Modelled, Observed_Rabida)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")

wb = openpyxl.Workbook()
sheet = wb.active

c1 = sheet.cell(row = 1, column = 1) 
c1.value = "Fechas"

sheet.column_dimensions["A"].width = 18

c1 = sheet.cell(row = 1, column = 2) 
c1.value = "El Carmen"

c1 = sheet.cell(row = 1, column = 3) 
c1.value = "La Rabida"

c1 = sheet.cell(row = 1, column = 4) 
c1.value = "CAMS"

for i in range(0, len(Modelled)):
    cx = sheet.cell(row = i+2, column = 1) 
    cx.value = Finish_Date[i]

    cy = sheet.cell(row = i+2, column = 2) 
    cy.value = Observed_Carmen[i]

    cz = sheet.cell(row = i+2, column = 3) 
    cz.value = Observed_Rabida[i]

    cp = sheet.cell(row = i+2, column = 4) 
    cp.value = Modelled[i]
    
wb.save("Resultados.xlsx")


plt.show()





