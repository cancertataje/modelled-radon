import cdsapi

c = cdsapi.Client()

c.retrieve(
    'cams-global-reanalysis-eac4',
    {
        'variable': 'radon',
        'model_level': '1', #top of the atmosphere
        'date': '2015-01-01/2021-12-30',
        'time': [
            '00:00', '03:00', '06:00',
            '09:00', '12:00', '15:00',
            '18:00', '21:00',
        ],
        'area': [
            90, -180, -90,
            180,

        ],
        'format': 'netcdf',
    },
    'radon.nc')
