import Read as Rd
import pandas as pd
import datetime
import numpy as np
from matplotlib import pyplot as plt

#--------- Insert Modelled Data ---------#
Dat_Model_Time_Carmen, Dat_Model_Rn_Carmen = Rd.File("Rn_CAMS_2015-2021.nc", 10, 4) # (37.2703 °N, -6.9243 °E)
Dat_Model_Time_Rabida, Dat_Model_Rn_Rabida = Rd.File("Rn_CAMS_2015-2021.nc", 10, 4) # (37.2029 °N, -6.9201 °E)
Dat_Model_Time_Murcia, Dat_Model_Rn_Murcia = Rd.File("Rn_CAMS_2015-2021.nc", 9, 12) # (38.0015 °N, -1.1707 °E)
Dat_Model_Time_Andujar, Dat_Model_Rn_Andujar = Rd.File("Rn_CAMS_2015-2021.nc", 9, 8) # (38.0250 ºN, -4.0618 ºE)
Dat_Model_Time_Motril, Dat_Model_Rn_Motril = Rd.File("Rn_CAMS_2015-2021.nc", 11, 9) # (36.7244 ºN, -3.5286 ºE)
Dat_Model_Time_Sevilla, Dat_Model_Rn_Sevilla = Rd.File("Rn_CAMS_2015-2021.nc", 10, 5) # (37.4003 ºN, -6.0097 ºE)
Dat_Model_Time_Herrera, Dat_Model_Rn_Herrera = Rd.File("Rn_CAMS_2015-2021.nc", 7, 7) # (39.1687 ºN, -5.0620 ºE)
Dat_Model_Time_Talavera, Dat_Model_Rn_Talavera = Rd.File("Rn_CAMS_2015-2021.nc", 8, 4) # (38.8811 ºN, -6.8151 ºE)
Dat_Model_Time_Huelva, Dat_Model_Rn_Huelva = Rd.File("Rn_CAMS_2015-2021.nc", 10, 4) # (37.2785 ºN, -6.9116 ºE)
Dat_Model_Time_Tarifa, Dat_Model_Rn_Tarifa = Rd.File("Rn_CAMS_2015-2021.nc", 12, 6) # (36.0138 ºN, -5.5987 ºE)
Dat_Model_Time_Mallorca, Dat_Model_Rn_Mallorca = Rd.File("Rn_CAMS_2015-2021.nc", 7, 17) # (39.5694 ºN, 2.6503 ºE)

Modelled_Carmen = Dat_Model_Rn_Carmen*6.9795*10**18
Modelled_Rabida = Dat_Model_Rn_Rabida*6.9795*10**18
Modelled_Murcia = Dat_Model_Rn_Murcia*6.9795*10**18
Modelled_Andujar = Dat_Model_Rn_Andujar*6.9795*10**18
Modelled_Motril = Dat_Model_Rn_Motril*6.9795*10**18
Modelled_Sevilla = Dat_Model_Rn_Sevilla*6.9795*10**18
Modelled_Herrera = Dat_Model_Rn_Herrera*6.9795*10**18
Modelled_Talavera = Dat_Model_Rn_Talavera*6.9795*10**18
Modelled_Huelva = Dat_Model_Rn_Huelva*6.9795*10**18
Modelled_Tarifa = Dat_Model_Rn_Tarifa*6.9795*10**18
Modelled_Mallorca = Dat_Model_Rn_Mallorca*6.9795*10**18

#--------- 2015-01-01 00:00:00

#--------- Insert Observed Data ---------#

Datos = pd.read_excel('Datos_Rn_2015-2018.xlsx')

Dat_Experimental_Date = Datos['Date'].to_numpy()
Dat_Experimental = Datos['Date'].astype(str)
Datos['Reference'] = '1900-01-01 00:00:00'
Datos['Reference'] = pd.to_datetime(Datos['Reference'])
Datos['Date'] = pd.to_datetime(Datos['Date'])


Datos['Minutes'] = ((Datos['Date'] - Datos['Reference']).dt.days * 24 +
                    (Datos['Date'] - Datos['Reference']).dt.seconds / 3600)

Datos['Minutes'] = Datos['Minutes'].astype(int)

Dat_Experimental_Time = Datos['Minutes'].to_numpy()

Datos.set_index(Datos['Minutes'], inplace=True)
Datos.drop(['Date', 'Minutes', 'Reference'],axis=1,inplace=True)

El_Carmen = Datos['El-Carmen'].to_numpy()
La_Rabida = Datos['La-Rabida'].to_numpy()
Murcia = Datos['Murcia'].to_numpy()
Andujar = Datos['Andujar'].to_numpy()
Motril = Datos['Motril'].to_numpy()
Sevilla = Datos['Sevilla'].to_numpy()
Herrera = Datos['Herrera'].to_numpy()
Talavera = Datos['Talavera'].to_numpy()
Huelva = Datos['Huelva'].to_numpy()
Tarifa = Datos['Tarifa'].to_numpy()
Mallorca = Datos['Palma-de-Mallorca'].to_numpy()

Dat_Model_Time = Dat_Model_Time_Carmen

Modelled_R_Carmen = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Rabida = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Murcia = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Andujar = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Motril = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Sevilla = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Herrera = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Talavera = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Huelva = np.zeros(len(Dat_Experimental_Time))
Modelled_R_Tarifa= np.zeros(len(Dat_Experimental_Time))
Modelled_R_Mallorca= np.zeros(len(Dat_Experimental_Time))

Finish_Date = np.zeros(len(Dat_Experimental_Time), dtype = object)

for i in range(0, len(Dat_Experimental_Time)):
    Position = np.array(np.where(Dat_Model_Time == Dat_Experimental_Time[i]))
    Position = np.array(Position[0])
    
    Modelled_R_Carmen[i] = Modelled_Carmen[int(Position)]
    Modelled_R_Rabida[i] = Modelled_Rabida[int(Position)]
    Modelled_R_Murcia[i] = Modelled_Murcia[int(Position)]
    Modelled_R_Andujar[i] = Modelled_Andujar[int(Position)]
    Modelled_R_Motril[i] = Modelled_Motril[int(Position)]
    Modelled_R_Sevilla[i] = Modelled_Sevilla[int(Position)]
    Modelled_R_Herrera[i] = Modelled_Herrera[int(Position)]
    Modelled_R_Talavera[i] = Modelled_Talavera[int(Position)]
    Modelled_R_Huelva[i] = Modelled_Huelva[int(Position)]
    Modelled_R_Tarifa[i] = Modelled_Tarifa[int(Position)]
    Modelled_R_Mallorca[i] = Modelled_Mallorca[int(Position)]
    Finish_Date[i] = Dat_Experimental[i]


Time = [datetime.datetime.strptime(date, "%Y-%m-%d %H:%M:%S").date() for date in Finish_Date]

#---------------- Comparation between both variables ----------------#
print("----------------------------------------------------------")
print("----------------- Results for Carmen ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Carmen = Rd.RMSE(Modelled_R_Carmen, El_Carmen)
MensajeBIAS = Rd.BIAS(Modelled_R_Carmen, El_Carmen)
MensajeIOA = Rd.IOA(Modelled_R_Carmen, El_Carmen)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")

print("----------------------------------------------------------")
print("----------------- Results for Rabida ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Rabida = Rd.RMSE(Modelled_R_Rabida, La_Rabida)
MensajeBIAS = Rd.BIAS(Modelled_R_Rabida, La_Rabida)
MensajeIOA = Rd.IOA(Modelled_R_Rabida, La_Rabida)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")    

print("----------------------------------------------------------")
print("----------------- Results for Murcia ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Murcia = Rd.RMSE(Modelled_R_Murcia, Murcia)
MensajeBIAS = Rd.BIAS(Modelled_R_Murcia, Murcia)
MensajeIOA = Rd.IOA(Modelled_R_Murcia, Murcia)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")       


print("----------------------------------------------------------")
print("----------------- Results for Andujar ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Andujar = Rd.RMSE(Modelled_R_Andujar, Andujar)
MensajeBIAS = Rd.BIAS(Modelled_R_Andujar, Andujar)
MensajeIOA = Rd.IOA(Modelled_R_Andujar, Andujar)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")

#plt.subplot(2, 2, 1)
plt.plot(Time, El_Carmen, '-b', label = 'Observed Carmen')
plt.plot(Time, Mod_Carmen, '-r', label = 'Modelled')
plt.title("Comparation in 'El Carmen'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Carmen, El_Carmen, '.r')
plt.title("'El Carmen'")
plt.ylim(0, 60)
plt.xlim(0, 60)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()


#plt.subplot(2, 2, 2)
plt.plot(Time, La_Rabida, '-b', label = 'Observed Rabida')
plt.plot(Time, Mod_Rabida, '-r', label = 'Modelled')
plt.title("Comparation in 'Rabida'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Rabida, La_Rabida, '.r')
plt.title("'La Rabida'")
plt.ylim(0, 50)
plt.xlim(0, 50)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()


#plt.subplot(2, 2, 3)
plt.plot(Time, Murcia, '-b', label = 'Observed Murcia')
plt.plot(Time, Mod_Murcia, '-r', label = 'Modelled')
plt.title("Comparation in 'Murcia'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Murcia, Murcia, '.r')
plt.title("'Murcia'")
plt.ylim(0, 30)
plt.xlim(0, 30)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()


#plt.subplot(2, 2, 4)
plt.plot(Time, Andujar, '-b', label = 'Observed Andujar')
plt.plot(Time, Mod_Andujar, '-r', label = 'Modelled')
plt.title("Comparation in 'Andújar'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Andujar, Andujar, '.r')
plt.title("'Andújar'")
plt.ylim(0, 180)
plt.xlim(0, 180)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()



#---------------- Comparation between both variables ----------------#
print("----------------------------------------------------------")
print("----------------- Results for Motril ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Motril = Rd.RMSE(Modelled_R_Motril, Motril)
MensajeBIAS = Rd.BIAS(Modelled_R_Motril, Motril)
MensajeIOA = Rd.IOA(Modelled_R_Motril, Motril)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")

print("----------------------------------------------------------")
print("----------------- Results for Sevilla ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Sevilla = Rd.RMSE(Modelled_R_Sevilla, Sevilla)
MensajeBIAS = Rd.BIAS(Modelled_R_Sevilla, Sevilla)
MensajeIOA = Rd.IOA(Modelled_R_Sevilla, Sevilla)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")    

print("----------------------------------------------------------")
print("----------------- Results for Herrera ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Herrera = Rd.RMSE(Modelled_R_Herrera, Herrera)
MensajeBIAS = Rd.BIAS(Modelled_R_Herrera, Herrera)
MensajeIOA = Rd.IOA(Modelled_R_Herrera, Herrera)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")       


print("----------------------------------------------------------")
print("----------------- Results for Talavera ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Talavera = Rd.RMSE(Modelled_R_Talavera, Talavera)
MensajeBIAS = Rd.BIAS(Modelled_R_Talavera, Talavera)
MensajeIOA = Rd.IOA(Modelled_R_Talavera, Talavera)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")

#plt.subplot(2, 2, 1)
plt.plot(Time, Motril, '-b', label = 'Observed Motril')
plt.plot(Time, Mod_Motril, '-r', label = 'Modelled')
plt.title("Comparation in 'Motril'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Motril, Motril, '.r')
plt.title("'Motril'")
plt.ylim(0, 60)
plt.xlim(0, 60)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()


#plt.subplot(2, 2, 2)
plt.plot(Time, Sevilla, '-b', label = 'Observed Sevilla')
plt.plot(Time, Mod_Sevilla, '-r', label = 'Modelled')
plt.title("Comparation in 'Sevilla'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Sevilla, Sevilla, '.r')
plt.title("'Sevilla'")
plt.ylim(0, 60)
plt.xlim(0, 60)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()


#plt.subplot(2, 2, 3)
plt.plot(Time, Herrera, '-b', label = 'Observed Herrera')
plt.plot(Time, Mod_Herrera, '-r', label = 'Modelled')
plt.title("Comparation in 'Herrera'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Herrera, Herrera, '.r')
plt.title("'Herrera'")
plt.ylim(0, 160)
plt.xlim(0, 160)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()


#plt.subplot(2, 2, 4)
plt.plot(Time, Talavera, '-b', label = 'Observed Talavera')
plt.plot(Time, Mod_Talavera, '-r', label = 'Modelled')
plt.title("Comparation in 'Talavera'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Talavera, Talavera, '.r')
plt.title("'Talavera'")
plt.ylim(0, 160)
plt.xlim(0, 160)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()



#---------------- Comparation between both variables ----------------#
print("----------------------------------------------------------")
print("----------------- Results for Huelva ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Huelva = Rd.RMSE(Modelled_R_Huelva, Huelva)
MensajeBIAS = Rd.BIAS(Modelled_R_Huelva, Huelva)
MensajeIOA = Rd.IOA(Modelled_R_Huelva, Huelva)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")

print("----------------------------------------------------------")
print("----------------- Results for Tarifa ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Tarifa = Rd.RMSE(Modelled_R_Tarifa, Tarifa)
MensajeBIAS = Rd.BIAS(Modelled_R_Tarifa, Tarifa)
MensajeIOA = Rd.IOA(Modelled_R_Tarifa, Tarifa)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")    

print("----------------------------------------------------------")
print("----------------- Results for Mallorca ---------------------")
print("----------------------------------------------------------")
print(" ")
MensajeRMSE, Mod_Mallorca = Rd.RMSE(Modelled_R_Mallorca, Mallorca)
MensajeBIAS = Rd.BIAS(Modelled_R_Mallorca, Mallorca)
MensajeIOA = Rd.IOA(Modelled_R_Mallorca, Mallorca)

print(MensajeRMSE)
print(MensajeBIAS)
print(MensajeIOA)
print(" ")

#plt.subplot(2, 2, 1)
plt.plot(Time, Huelva, '-b', label = 'Observed Huelva')
plt.plot(Time, Mod_Huelva, '-r', label = 'Modelled')
plt.title("Comparation in 'Huelva'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Huelva, Huelva, '.r')
plt.title("'Huelva'")
plt.ylim(0, 80)
plt.xlim(0, 80)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()


#plt.subplot(2, 2, 2)
plt.plot(Time, Tarifa, '-b', label = 'Observed Tarifa')
plt.plot(Time, Mod_Tarifa, '-r', label = 'Modelled')
plt.title("Comparation in 'Tarifa'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Tarifa, Tarifa, '.r')
plt.title("'Tarifa'")
plt.ylim(0, 60)
plt.xlim(0, 60)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()

#plt.subplot(2, 2, 3)
plt.plot(Time, Mallorca, '-b', label = 'Observed Mallorca')
plt.plot(Time, Mod_Mallorca, '-r', label = 'Modelled')
plt.title("Comparation in 'Palma de Mallorca'")
plt.ylabel("Activity (Bq.m-3)")
plt.xlabel("Dates (Year-Month)")
plt.legend(loc='upper right', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)
plt.grid()
plt.show()

plt.plot(Mod_Mallorca, Mallorca, '.r')
plt.title("'Palma de Mallorca'")
plt.ylim(0, 140)
plt.xlim(0, 140)
plt.xlabel("Valores modelados (Bq.m-3)")
plt.ylabel("Valores observados (Bq.m-3)")
plt.grid()
plt.show()




















