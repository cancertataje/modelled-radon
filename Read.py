from netCDF4 import Dataset as NetCDFFile
import numpy as np

def File(name, Pos_Lat, Pos_Long):
    try:
        Pos_Lat = int(Pos_Lat)
        Pos_Long = int(Pos_Long)
        file = NetCDFFile(name, 'r')        
        Value_Time = file['time'][:]
        Value_Rn = file['ra'][:,Pos_Lat,Pos_Long]
        #print(file.variables['latitude'][:])
        #print(file.variables['longitude'][:])        
        return Value_Time, Value_Rn

    except Exception as e:
        print(e)


def RMSE(Modelled, Observed):    
    Modelled = np.array(Modelled)
    Observed = np.array(Observed)
    
    NAN = np.array(np.where(np.isnan(Observed) == True))
    NAN = np.array(NAN[0])    
    N = len(Modelled) - len(NAN)
    
    RMSE = 0
    for i in range(0, len(Observed)):
        if (np.isnan(Observed[i]) != True):            
            RMSE = RMSE + (Modelled[i] - Observed[i])**2
        else:
            Modelled[i] = np.nan

    RMSE = np.sqrt(RMSE / N)

    menssage = "The root mean square error is of "+str(round(RMSE, 3))+" Bq / m**3."
    
    return menssage, Modelled

def BIAS(Modelled, Observed):
    Modelled = np.array(Modelled)
    Observed = np.array(Observed)

    NAN = np.array(np.where(np.isnan(Observed) == True))
    NAN = np.array(NAN[0])
    N = len(Modelled) - len(NAN)

    AverageM = 0
    AverageO = 0
    for i in range(0, len(Observed)):
        if (np.isnan(Observed[i]) != True):
            AverageM = AverageM + Modelled[i]
            AverageO = AverageO + Observed[i]
    
    AverageM = AverageM / N
    AverageO = AverageO / N
    
    BIAS = AverageM - AverageO

    menssage = "The BIAS is of "+str(round(BIAS, 3))+" Bq / m**3."
    return menssage

def IOA(Modelled, Observed):
    Modelled = np.array(Modelled)
    Observed = np.array(Observed)
    
    NAN = np.array(np.where(np.isnan(Observed) == True))
    NAN = np.array(NAN[0])
    N = len(Modelled) - len(NAN)
    
    AverageO = 0
    for i in range(0, len(Observed)):
        if (np.isnan(Observed[i]) != True):            
            AverageO = AverageO + Observed[i]
    
    AverageO = AverageO / N

    Numerator = 0
    Denominator = 0

    for i in range(0, len(Observed)):
        if (np.isnan(Observed[i]) != True):
            Numerator = Numerator + (Modelled[i] - Observed[i])**2
            Denominator = Denominator + (np.abs(Modelled[i] - AverageO) +
                                         np.abs(Observed[i] - AverageO))**2

    IOA = 1 - Numerator / Denominator

    menssage = "The IOA is of "+str(round(IOA, 3))

    return menssage
  
